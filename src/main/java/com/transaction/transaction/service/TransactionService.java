package com.transaction.transaction.service;

import com.transaction.transaction.model.Transaction;
import com.transaction.transaction.model.dao.TransactionInput;
import com.transaction.transaction.model.dao.TransactionListResponse;

import java.util.List;

public interface TransactionService {

    void createTransaction(TransactionInput transactionInput);
    TransactionListResponse getAll(int page, int size);
    void addMultipleTransactions(List<TransactionInput> tnxs);
}
