# Transactions Backend Challenge(Challenge)

Transaction challenge is a feature that allows reading of multiple transactions with pagination 

## Installation
Run the following command in the root directory of the project to package it
```bash
$ mvn clean package
```

## With Docker
Now build the docker image by typing the following command

```bash
docker build -f Dockerfile -t transaction-challenge . 
```

and run the image

```bash
docker run -p 7888:7888 transaction-challenge
```

## Without Docker
Navigate to the root of the project via command line and execute the command

```bashclear
mvn spring-boot:run
```


## Swagger

```link
http://localhost:7888/swagger-ui.html#/
```


## License
[MIT](https://choosealicense.com/licenses/mit/)







