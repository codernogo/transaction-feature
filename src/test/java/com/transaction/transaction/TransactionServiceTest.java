package com.transaction.transaction;

import com.transaction.transaction.model.Type;
import com.transaction.transaction.model.dao.TransactionInput;
import com.transaction.transaction.model.dao.TransactionListResponse;
import com.transaction.transaction.service.TransactionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@Transactional
public class TransactionServiceTest {

    @Autowired
    private TransactionService transactionService;

    @Test
    void FindAll_ReturnsAPagedListOfTasks() {


        TransactionInput tnx1 = new TransactionInput("3f36bdr7-d3f0-48ce-84e6-0f90a4ed7cc0", "19/07/2021", Type.CPT,"ATM",20,0,"390.40");
        TransactionInput tnx2 = new TransactionInput("3f36bddd7-d3f0-48ce-84e6-0f90a4ed7cc0", "17/07/2021", Type.CPT,"ATM",20,0,"390.40");
        TransactionInput tnx3 = new TransactionInput("3f36b5d7-d3f0-48ce-84e6-0f90a4ed7cc0", "15/07/2021", Type.CPT,"ATM",20,0,"390.40");
        TransactionInput tnx4 = new TransactionInput("3f36bud7-d3f0-48ce-84e6-0f90a4ed7cc0", "10/07/2021", Type.CPT,"ATM",20,0,"390.40");

        transactionService.createTransaction(tnx1);
        transactionService.createTransaction(tnx2);
        transactionService.createTransaction(tnx3);
        transactionService.createTransaction(tnx4);

        TransactionListResponse tnx = transactionService.getAll(0,4);

        assertThat(tnx.getTransactionList().size(),equalTo(4));

    }


}
